package br.com.imagination.boleto.controller;

import br.com.imagination.boleto.dto.BoletoDTO;
import org.apache.commons.lang.time.DateUtils;
import org.jrimum.bopepo.BancosSuportados;
import org.jrimum.bopepo.Boleto;
import org.jrimum.bopepo.parametro.ParametroBancoSicredi;
import org.jrimum.domkee.comum.pessoa.endereco.Endereco;
import org.jrimum.domkee.comum.pessoa.endereco.UnidadeFederativa;
import org.jrimum.domkee.financeiro.banco.ParametrosBancariosMap;
import org.jrimum.domkee.financeiro.banco.febraban.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utils.Exemplos;
import java.util.Date;

@RestController
@RequestMapping("/boleto")
public class BoletoController {

    @GetMapping
    public ResponseEntity<String> index() {
        return ResponseEntity.ok("API Boleto");
    }

    @GetMapping("/{id}")
    public ResponseEntity<String> show(@PathVariable String id) {
        StringBuilder builder = new StringBuilder();

        builder.append("API Boleto ");
        builder.append(id);

        return ResponseEntity.ok(builder.toString());
    }

    @PostMapping
    public ResponseEntity<String> created(@RequestBody BoletoDTO request, @RequestHeader HttpHeaders headers) {
        String token = AuthController.getTokenInHeaders(headers);
        Cedente cedente = AuthController.getCedenteByToken(token);
        if(cedente.getNome().equals("")) {
            return ResponseEntity
                    .badRequest()
                    .body("Cedente não encontrado!");
        }
        Sacado sacado = new Sacado(request.getSacado().getNome(), request.getSacado().getDocumento());

        BancosSuportados bancoSuportado = findBancoByCodigo(request.getConta().getBanco());
        if(bancoSuportado == null) {
            return ResponseEntity
                    .badRequest()
                    .body("Banco informado é inválido!");
        }
        ContaBancaria contaBancaria = new ContaBancaria();
        contaBancaria.setBanco(bancoSuportado.create());
        contaBancaria.setAgencia(new Agencia(request.getConta().getAgencia()));
        contaBancaria.setNumeroDaConta(new NumeroDaConta(request.getConta().getNumeroDaConta()));
        contaBancaria.setCarteira(new Carteira(request.getConta().getCarteira(), TipoDeCobranca.COM_REGISTRO));

        Endereco endereco = new Endereco();
        endereco.setLogradouro(request.getSacado().getLogradouro());
        endereco.setNumero(request.getSacado().getNumero());
        endereco.setComplemento(request.getSacado().getComplemento());
        endereco.setBairro(request.getSacado().getBairro());
        endereco.setCep(request.getSacado().getCep());
        endereco.setLocalidade(request.getSacado().getCidade());
        endereco.setUF(UnidadeFederativa.valueOfSigla(request.getSacado().getUf()));
        sacado.addEndereco(endereco);

        Titulo titulo = new Titulo(contaBancaria, sacado, cedente);
        titulo.setNossoNumero(request.getConta().getNossoNumero());
        titulo.setDigitoDoNossoNumero(request.getConta().getDigitoDoNossoNumero());
        titulo.setParametrosBancarios(new ParametrosBancariosMap(ParametroBancoSicredi.POSTO_DA_AGENCIA, 2));
        titulo.setValor(request.getValor());
        titulo.setDesconto(request.getDesconto());
        titulo.setValorCobrado(request.getValor().subtract(request.getDesconto()));
        titulo.setDataDoDocumento(new Date());
        titulo.setDataDoVencimento(DateUtils.addDays(new Date(), 3));
        titulo.setTipoDeDocumento(TipoDeTitulo.DM_DUPLICATA_MERCANTIL);
        titulo.setNumeroDoDocumento("123");

        Boleto boleto = Exemplos.crieBoleto(titulo);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(Exemplos.executePDF(boleto).toString());

    }

    @PutMapping("/{id}")
    public ResponseEntity<String> update(@RequestBody BoletoDTO request, @PathVariable String id) {
        StringBuilder builder = new StringBuilder();

        builder.append("API Boleto ");
        builder.append(id);
        builder.append(" ");
        //builder.append(request.getTitulo());

        return ResponseEntity.ok(builder.toString());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity destroy(@PathVariable String id) {
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .body("");
    }

    private BancosSuportados findBancoByCodigo(String idBanco){
        BancosSuportados[] bancosSuportados = BancosSuportados.values();
        for (BancosSuportados bancoSuportado : bancosSuportados) {
            if(bancoSuportado.getCodigoDeCompensacao().equals(idBanco))
                return bancoSuportado;
        }
        return null;
    }
}