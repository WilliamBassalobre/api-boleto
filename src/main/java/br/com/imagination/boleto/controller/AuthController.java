package br.com.imagination.boleto.controller;

import org.jrimum.domkee.financeiro.banco.febraban.Cedente;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/login")
public class AuthController {

    @GetMapping
    public ResponseEntity<String> index() {
        return ResponseEntity.ok("API Boleto");
    }

    @GetMapping("/me")
    public ResponseEntity<String> showCedente(@RequestHeader HttpHeaders headers) {
        String token = getTokenInHeaders(headers);
        Cedente cedente = getCedenteByToken(token);
        return ResponseEntity.ok(cedente.toString());
    }

    @PostMapping
    public ResponseEntity<String> created(@RequestBody String request) {
        System.out.println(request);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(request);

    }

    public static Cedente getCedenteByToken(String token){
        Cedente cedente = new Cedente("");
        if(token.equals("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.XbPfbIHMI6arZ3Y922BhjWgQzWXcXNrz0ogtVhfEd2o")) {
            cedente = new Cedente("Drimie", "47.988.663/0001-20");
        }
        return cedente;
    }

    public static String getTokenInHeaders(HttpHeaders headers){
        String auth = "";
        List<String> list = headers.get("Authorization");
        if(list.size() > 0)
            auth = list.get(0).replace("Bearer ", "");
        return auth;
    }
}
