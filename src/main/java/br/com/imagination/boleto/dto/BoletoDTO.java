package br.com.imagination.boleto.dto;

import java.math.BigDecimal;

public class BoletoDTO {
    private CedenteDTO cedente;
    private SacadoDTO sacado;
    private ContaDTO conta;
    private BigDecimal valor;
    private BigDecimal desconto;

    public CedenteDTO getCedente() {
        return cedente;
    }

    public void setCedente(CedenteDTO cedente) {
        this.cedente = cedente;
    }

    public SacadoDTO getSacado() {
        return sacado;
    }

    public void setSacado(SacadoDTO sacado) {
        this.sacado = sacado;
    }

    public ContaDTO getConta() {
        return conta;
    }

    public void setConta(ContaDTO conta) {
        this.conta = conta;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getDesconto() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto = desconto;
    }
}