package br.com.imagination.boleto.dto;

public class ContaDTO {
    private String banco;
    private int agencia;
    private int numeroDaConta;
    private int carteira;
    private String nossoNumero;
    private String digitoDoNossoNumero;

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public int getAgencia() {
        return agencia;
    }

    public void setAgencia(int agencia) {
        this.agencia = agencia;
    }

    public int getNumeroDaConta() {
        return numeroDaConta;
    }

    public void setNumeroDaConta(int numeroDaConta) {
        this.numeroDaConta = numeroDaConta;
    }

    public int getCarteira() {
        return carteira;
    }

    public void setCarteira(int carteira) {
        this.carteira = carteira;
    }

    public String getNossoNumero() {
        return nossoNumero;
    }

    public void setNossoNumero(String nossoNumero) {
        this.nossoNumero = nossoNumero;
    }

    public String getDigitoDoNossoNumero() {
        return digitoDoNossoNumero;
    }

    public void setDigitoDoNossoNumero(String digitoDoNossoNumero) {
        this.digitoDoNossoNumero = digitoDoNossoNumero;
    }
}
