API de geração de boletos.

Para testar, use o seguinte JSON no endpoint v1/boleto, usando o verbo POST:
```
{
	"sacado": {
		"nome": "Guilherme Marques",
		"documento": "071.018.599-50",
		"logradouro": "Av. José Alves  Nendo",
		"numero": "1256",
		"complemento": "Bl. 45 Ap. 03",
		"bairro": "Jd. São Silvestre",
		"cidade": "Maringá",
		"uf": "PR"
	},
	"conta": {
		"banco": "748",
		"agencia": "166",
		"numeroDaConta": "623",
		"carteira": "1",
		"nossoNumero": "07200003",
		"digitoDoNossoNumero": "1"
	},
	"valor": 250.30,
	"desconto": 12.25
}
```
Deve ser acresentado o Header Authorization na requisição, com o seguinte conteúdo:
Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.XbPfbIHMI6arZ3Y922BhjWgQzWXcXNrz0ogtVhfEd2o